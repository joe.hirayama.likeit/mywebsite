package beans;

public class AvgSaunaRateBeans {
  Float avgRate;
  int saunaId;

  public AvgSaunaRateBeans(Float avgRate, int saunaId) {
    super();
    this.avgRate = avgRate;
    this.saunaId = saunaId;
  }

  public AvgSaunaRateBeans() {}

  public Float getAvgRate() {
    return avgRate;
  }

  public void setAvgRate(Float avgRate) {
    this.avgRate = avgRate;
  }

  public int getSaunaId() {
    return saunaId;
  }

  public void setSaunaId(int saunaId) {
    this.saunaId = saunaId;
  }


}


