package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class CommentBeans implements Serializable {
  private int id;
  private int userId;
  private int saunaId;
  private String post_name;
  private int rate;
  private Timestamp createDate;
  private String body;

  public CommentBeans() {}


  public CommentBeans(int id, int userId, int saunaId, String post_name, int rate,
      Timestamp createDate, String body) {
    super();
    this.id = id;
    this.userId = userId;
    this.saunaId = saunaId;
    this.post_name = post_name;
    this.rate = rate;
    this.createDate = createDate;
    this.body = body;
  }


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public int getUserId() {
    return userId;
  }


  public void setUserId(int userId) {
    this.userId = userId;
  }


  public int getSaunaId() {
    return saunaId;
  }


  public void setSaunaId(int saunaId) {
    this.saunaId = saunaId;
  }


  public String getPost_name() {
    return post_name;
  }


  public void setPost_name(String post_name) {
    this.post_name = post_name;
  }


  public int getRate() {
    return rate;
  }


  public void setRate(int rate) {
    this.rate = rate;
  }


  public Timestamp getCreateDate() {
    return createDate;
  }


  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }


  public String getBody() {
    return body;
  }


  public void setBody(String body) {
    this.body = body;
  }


}
