package beans;

import java.io.Serializable;

public class SaunaBeans implements Serializable {
  private int id;
  private String name;
  private String location;
  private int price;
  private int sauna_temp;
  private int water_temp;
  private float avgRate;


  public SaunaBeans() {}


  public SaunaBeans(int id, String name, String location, int price, int sauna_temp,
      int water_temp, float avgRate) {
    super();
    this.id = id;
    this.name = name;
    this.location = location;
    this.price = price;
    this.sauna_temp = sauna_temp;
    this.water_temp = water_temp;
    this.avgRate = avgRate;

  }


  public float getAvgRate() {
    return avgRate;
  }


  public void setAvgRate(float avgRate) {
    this.avgRate = avgRate;
  }


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }





  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public String getLocation() {
    return location;
  }


  public void setLocation(String location) {
    this.location = location;
  }


  public int getPrice() {
    return price;
  }


  public void setPrice(int price) {
    this.price = price;
  }


  public int getSauna_temp() {
    return sauna_temp;
  }


  public void setSauna_temp(int sauna_temp) {
    this.sauna_temp = sauna_temp;
  }


  public int getWater_temp() {
    return water_temp;
  }


  public void setWater_temp(int water_temp) {
    this.water_temp = water_temp;
  }


}
