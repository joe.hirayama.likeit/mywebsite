package beans;

import java.io.Serializable;
import java.util.Date;

public class UserBeans implements Serializable {
  private int id;
  private String loginId;
  private String name;
  private Date birthDate;
  private String password;
  private boolean isAdmin;
  
  public UserBeans() {}

  public UserBeans(int id, String loginId, String name, Date birthDate, String password,
      boolean isAdmin) {
    super();
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.isAdmin = isAdmin;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isAdmin() {
    return isAdmin;
  }

  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }



}
