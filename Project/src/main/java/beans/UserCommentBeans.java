package beans;

public class UserCommentBeans {
  String saunaName;
  int rate;
  String body;
  int saunaId;
  int commentId;

  public UserCommentBeans(String saunaName, int rate, String body, int saunaId, int commentId) {
    super();
    this.saunaName = saunaName;
    this.rate = rate;
    this.body = body;
    this.saunaId = saunaId;
    this.commentId = commentId;

  }


  public UserCommentBeans() {}


  public String getSaunaName() {
    return saunaName;
  }


  public void setSaunaName(String saunaName) {
    this.saunaName = saunaName;
  }


  public int getRate() {
    return rate;
  }


  public void setRate(int rate) {
    this.rate = rate;
  }


  public String getBody() {
    return body;
  }


  public void setBody(String body) {
    this.body = body;
  }


  public int getSaunaId() {
    return saunaId;
  }


  public void setSaunaId(int saunaId) {
    this.saunaId = saunaId;
  }


  public int getCommentId() {
    return commentId;
  }


  public void setCommentId(int commentId) {
    this.commentId = commentId;
  }



}
