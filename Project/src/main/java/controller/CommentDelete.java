package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.CommentBeans;
import beans.SaunaBeans;
import beans.UserBeans;
import dao.CommentDao;
import dao.SaunaDao;

/**
 * Servlet implementation class CommentDelete
 */
@WebServlet("/CommentDelete")
public class CommentDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("userInfo");
      if (u == null) {
        // リダイレクトのコードを書く
        response.sendRedirect("Login");
        return;
      }

      // リクエストパラメータの入力項目を取得
      String __id = request.getParameter("saunaId");
      int saunaId = 0;
      try {
        saunaId = Integer.valueOf(__id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      // リクエストパラメータの入力項目を取得
      String _id = request.getParameter("commentId");
      int commentId = 0;
      try {
        commentId = Integer.valueOf(_id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }

      SaunaDao sd = new SaunaDao();
      SaunaBeans sauna = sd.findById(saunaId);

      CommentDao cd = new CommentDao();
      CommentBeans comment = cd.findCommentByCommentId(commentId);

      // リクエストスコープにsauna情報をセット
      request.setAttribute("sauna", sauna);
      // リクエストスコープにcomment情報をセット
      request.setAttribute("comment", comment);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/commentDelete.jsp");
      dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String _id = request.getParameter("commentId");
      int commentId = 0;
      try {
        commentId = Integer.valueOf(_id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      // リクエストパラメータの入力項目を取得
      String __id = request.getParameter("saunaId");
      int saunaId = 0;
      try {
        saunaId = Integer.valueOf(__id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }


      CommentDao cd = new CommentDao();
      cd.deleteComment(commentId);
      response.sendRedirect("SaunaDetail?id=" + saunaId);

	}

}
