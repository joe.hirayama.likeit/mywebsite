package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.UserDao;
import util.PasswordEncoder;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserBeans user = (UserBeans) session.getAttribute("userInfo");
      if (user != null) {
        // リダイレクトのコードを書く
        response.sendRedirect("SaunaList");
        return;
      }

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
      dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("loginId");
      String password = request.getParameter("password");

      // リクエストパラメータの入力項目を引数に渡して、PasswordEncoderのメソッドを実行
      PasswordEncoder p = new PasswordEncoder();
      String encodedPassword = p.encodePassword(password);

      // リクエストパラメータの入力項目を引数に渡して、UserDaoのメソッドを実行
      UserDao userDao = new UserDao();
      UserBeans userBeans = userDao.findByLoginInfo(loginId, encodedPassword);

      /** テーブルに該当のデータが見つからなかった場合 * */
      if (userBeans == null) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
        // 入力したログインIDを画面に表示するため、リクエストに値をセット
        request.setAttribute("loginId", loginId);
        // ログインjspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        dispatcher.forward(request, response);
        return;
      }

      /** テーブルに該当のデータが見つかった場合 * */
      // セッションにユーザの情報をセット
      HttpSession session = request.getSession();
      session.setAttribute("userInfo", userBeans);

      response.sendRedirect("SaunaList");

	}

}
