package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.CommentDao;

/**
 * Servlet implementation class PostComment
 */
@WebServlet("/PostComment")
public class PostComment extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public PostComment() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインセッションがない場合、ログイン画面にリダイレクトさせる
    HttpSession session = request.getSession();
    UserBeans u = (UserBeans) session.getAttribute("userInfo");
    if (u == null) {
      // リダイレクトのコードを書く
      response.sendRedirect("Login");
      return;
    }
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String _id = request.getParameter("id");
    int id = 0;
    try {
      id = Integer.valueOf(_id);
    } catch (NumberFormatException ex) {
      ex.printStackTrace();
    }
    // リクエストスコープにsauna情報をセット
    request.setAttribute("saunaId", id);
    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/postComment.jsp");
    dispatcher.forward(request, response);
  }

  /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String _id = request.getParameter("userId");
      int userId = 0;
      try {
        userId = Integer.valueOf(_id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }

      String __id = request.getParameter("saunaId");
      int saunaId = 0;
      try {
        saunaId = Integer.valueOf(__id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      String postName = request.getParameter("userName");
      String _rate = request.getParameter("rate");

      String ___id = request.getParameter("saunaId");

      int rate = 0;
      try {
        rate = Integer.valueOf(_rate);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      String body = request.getParameter("body");


      if (body.equals("")) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力内容にエラーがあります。");
        request.setAttribute("saunaId", saunaId);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/postComment.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // commentDaoでinsertする
      CommentDao cd = new CommentDao();
      cd.insertComment(userId, saunaId, postName, rate, body);

      response.sendRedirect("SaunaDetail?id=" + saunaId);
     
	}

}
