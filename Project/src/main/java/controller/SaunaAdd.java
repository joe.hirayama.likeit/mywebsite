package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.SaunaDao;

/**
 * Servlet implementation class SaunaAdd
 */
@WebServlet("/SaunaAdd")
public class SaunaAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaunaAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("userInfo");
      if (u == null) {
        // リダイレクトのコードを書く
        response.sendRedirect("Login");
        return;
      }
      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/saunaAdd.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String _name = request.getParameter("name");
      String _location = request.getParameter("location");
      String _price = request.getParameter("price");
      String _sauna_temp = request.getParameter("sauna_temp");
      String _water_temp = request.getParameter("water_temp");


      if (_name.equals("") || _location.equals("") || _price.equals("") || _sauna_temp.equals("")
          || _water_temp.equals("")) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/saunaAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }


      int price = 0;
      try {
        price = Integer.valueOf(_price);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      int sauna_temp = 0;
      try {
        sauna_temp = Integer.valueOf(_sauna_temp);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      int water_temp = 0;
      try {
        water_temp = Integer.valueOf(_water_temp);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }



      SaunaDao sd = new SaunaDao();
      sd.insertSauna(_name, _location, price, sauna_temp, water_temp);

      response.sendRedirect("SaunaList");

	}

}
