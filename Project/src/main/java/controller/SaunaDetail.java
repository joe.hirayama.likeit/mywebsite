package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.CommentBeans;
import beans.SaunaBeans;
import beans.UserBeans;
import dao.CommentDao;
import dao.SaunaDao;

/**
 * Servlet implementation class SaunaDetail
 */
@WebServlet("/SaunaDetail")
public class SaunaDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaunaDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("userInfo");
      if (u == null) {
        // リダイレクトのコードを書く
        response.sendRedirect("Login");
        return;
      }
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String _id = request.getParameter("id");
      int id = 0;
      try {
        id = Integer.valueOf(_id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }

      SaunaDao sd = new SaunaDao();
      SaunaBeans sauna = sd.findById(id);

      CommentDao cd = new CommentDao();
      List<CommentBeans> commentList = cd.findCommentBySaunaId(id);
      Float rate = cd.findAvgSaunaRateBySaunaId(id);

      // リクエストスコープにsauna情報をセット
      request.setAttribute("sauna", sauna);
      // リクエストスコープにcomment情報をセット
      request.setAttribute("commentList", commentList);
      request.setAttribute("rate", rate);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/saunaDetail.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
