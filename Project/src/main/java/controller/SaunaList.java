package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.AvgSaunaRateBeans;
import beans.SaunaBeans;
import beans.UserBeans;
import dao.CommentDao;
import dao.SaunaDao;

/**
 * Servlet implementation class SaunaList
 */
@WebServlet("/SaunaList")
public class SaunaList extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public SaunaList() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインセッションがない場合、ログイン画面にリダイレクトさせる
    HttpSession session = request.getSession();
    UserBeans u = (UserBeans) session.getAttribute("userInfo");
    if (u == null) {
      // リダイレクトのコードを書く
      response.sendRedirect("Login");
      return;
    }

    SaunaDao s = new SaunaDao();
    List<SaunaBeans> saunaList = s.findAllSauna();

    CommentDao cd = new CommentDao();
    List<AvgSaunaRateBeans> avgSaunaRateList = cd.selectAvgSaunaRate();


    for (int i = 0; i < avgSaunaRateList.size(); i++) {
      if (saunaList.get(i).getId() == avgSaunaRateList.get(i).getSaunaId()) {
        saunaList.get(i).setAvgRate(avgSaunaRateList.get(i).getAvgRate());
      }
    }


    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("saunaList", saunaList);

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/saunaList.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String _name = request.getParameter("name");
    String _location = request.getParameter("location");
    String _priceStart = request.getParameter("price-start");
    String _priceEnd = request.getParameter("price-end");


    // if (_name.equals("") && _location.equals("") && _priceStart.equals("")
    // && _priceEnd.equals("")) {
    // response.sendRedirect("SaunaList");
    // return;
    // }


    SaunaDao sd = new SaunaDao();
    List<SaunaBeans> saunaList = sd.search(_name, _location, _priceStart, _priceEnd);

    // for (SaunaBeans a : saunaList) {
    // System.out.println(a.getName());
    // }

    CommentDao cd = new CommentDao();
    List<AvgSaunaRateBeans> avgSaunaRateList = cd.selectAvgSaunaRate();
    for (int i = 0; i < saunaList.size(); i++) {
      for (int j = 0; j < avgSaunaRateList.size(); j++) {
        if (saunaList.get(i).getId() == avgSaunaRateList.get(j).getSaunaId()) {
          saunaList.get(i).setAvgRate(avgSaunaRateList.get(j).getAvgRate());
        }
      }
    }

    // リクエストスコープにsearchしたサウナ一覧情報をセット
    request.setAttribute("saunaList", saunaList);
    request.setAttribute("_name", _name);
    request.setAttribute("_location", _location);
    request.setAttribute("_priceStart", _priceStart);
    request.setAttribute("_priceEnd", _priceEnd);


    // sauna一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/saunaList.jsp");
    dispatcher.forward(request, response);

  }

}
