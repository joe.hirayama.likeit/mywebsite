package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.SaunaBeans;
import beans.UserBeans;
import dao.SaunaDao;

/**
 * Servlet implementation class SaunaUpdate
 */
@WebServlet("/SaunaUpdate")
public class SaunaUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaunaUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("userInfo");
      if (u == null) {
        // リダイレクトのコードを書く
        response.sendRedirect("Login");
        return;
      }

      // リクエストパラメータの入力項目を取得
      String _id = request.getParameter("id");
      int id = 0;
      try {
        id = Integer.valueOf(_id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      SaunaDao sd = new SaunaDao();
      SaunaBeans sauna = sd.findById(id);


      // リクエストスコープにsauna情報をセット
      request.setAttribute("sauna", sauna);
      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/saunaUpdate.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      SaunaDao sd = new SaunaDao();

      // リクエストパラメータの入力項目を取得
      String _id = request.getParameter("id");
      String name = request.getParameter("name");
      String location = request.getParameter("location");
      String price = request.getParameter("price");
      String sauna_temp = request.getParameter("sauna_temp");
      String water_temp = request.getParameter("water_temp");
      int id = 0;
      try {
        id = Integer.valueOf(_id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }

      if (_id.equals("") || name.equals("") || location.equals("") || price.equals("")
          || sauna_temp.equals("") || water_temp.equals("")) {

        request.setAttribute("errMsg", "入力された内容が正しくありません。");
        SaunaBeans sauna = sd.findById(id);
        request.setAttribute("sauna", sauna);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/saunaUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }
      sd.updateAll(id, name, location, price, sauna_temp, water_temp);
      response.sendRedirect("SaunaList");



	}



}
