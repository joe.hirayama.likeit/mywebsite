package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAdd")
public class UserAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("loginId");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");
      String userName = request.getParameter("name");
      String birthDate = request.getParameter("birth-date");

      UserDao ud = new UserDao();

      if (loginId.equals("") || userName.equals("") || birthDate.equals("") || password.equals("")
          || passwordConfirm.equals("") || !password.equals(passwordConfirm)) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "エラーメッセージ");
        // 入力した情報を画面に表示するため、リクエストに値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("userName", userName);
        request.setAttribute("birthDate", birthDate);

        // userAdd.jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;

      } else {
        PasswordEncoder p = new PasswordEncoder();
        String encodedPassword = p.encodePassword(password);

        // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        ud.insert(loginId, encodedPassword, userName, birthDate);
        response.sendRedirect("Login");
        return;
      }
	}

}
