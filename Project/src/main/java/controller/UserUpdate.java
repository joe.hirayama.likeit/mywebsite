package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.UserDao;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("userInfo");
      if (u == null) {
        // リダイレクトのコードを書く
        response.sendRedirect("Login");
        return;
      }
      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String _id = request.getParameter("user-id");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");
      String userName = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");

      int id = 0;
      try {
        id = Integer.valueOf(_id);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      // リクエストパラメータの入力項目を引数に渡して、UserDaoのメソッドを実行
      UserDao userDao = new UserDao();

      if (!password.equals(passwordConfirm) || userName.equals("") || birthDate.equals("")) {
        // 更新失敗。リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容が正しくありません");

        // リクエストスコープにユーザ一情報をセット
        UserBeans user = userDao.findById(id);
        request.setAttribute("user", user);
        // request.setAttribute("errUserName", userName);
        // request.setAttribute("errUserBirthDate", birthDate);

        // ユーザ一更新のjspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;

      } else if (password.equals("") && passwordConfirm.equals("")) {
        // password以外の情報をupdate
        userDao.updateExceptPassword(id, userName, birthDate);

      } else {
        PasswordEncoder p = new PasswordEncoder();
        String encodedPassword = p.encodePassword(password);
        // 全てupdate
        userDao.updateAll(id, encodedPassword, userName, birthDate);
      }
      UserBeans user = userDao.findById(id);
      HttpSession session = request.getSession();
      session.setAttribute("userInfo", user);
      // リダイレクト
      response.sendRedirect("UserDetail");
    }


}
