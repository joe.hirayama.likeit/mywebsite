package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.AvgSaunaRateBeans;
import beans.CommentBeans;
import beans.UserCommentBeans;

public class CommentDao {
  public List<CommentBeans> findCommentBySaunaId(int id) {
    Connection conn = null;
    List<CommentBeans> commentList = new ArrayList<CommentBeans>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "select * from comment where sauna_id =?";

      // SELECTを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();


      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int _id = rs.getInt("id");
        int userId = rs.getInt("user_id");
        int saunaId = rs.getInt("sauna_id");
        String postName = rs.getString("post_name");
        Timestamp createDate = rs.getTimestamp("create_date");
        int rate = rs.getInt("rate");
        String body = rs.getString("body");

        CommentBeans comment =
            new CommentBeans(_id, userId, saunaId, postName, rate, createDate, body);
        commentList.add(comment);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return commentList;
  }

  public void insertComment(int userId, int saunaId, String name, int rate, String body) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // insert文を準備
      String sql = "insert into comment(user_id,sauna_id,post_name,create_date,rate,body) \n"
          + "values(\n" + "?,?,?,now(),?,?\n" + ");";

      // insertを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      pStmt.setInt(2, saunaId);
      pStmt.setString(3, name);
      pStmt.setInt(4, rate);
      pStmt.setString(5, body);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void deleteComment(int id) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // Delete文を準備
      String sql = "DELETE FROM comment WHERE id = ?;";

      // Deleteを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      int rs = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public CommentBeans findCommentByCommentId(int id) {
    Connection conn = null;
    CommentBeans cb = new CommentBeans();
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "select * from comment where id =?";

      // SELECTを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int _id = rs.getInt("id");
        int userId = rs.getInt("user_id");
        int saunaId = rs.getInt("sauna_id");
        String postName = rs.getString("post_name");
        Timestamp createDate = rs.getTimestamp("create_date");
        int rate = rs.getInt("rate");
        String body = rs.getString("body");
        cb.setId(_id);
        cb.setUserId(userId);
        cb.setSaunaId(saunaId);
        cb.setPost_name(postName);
        cb.setCreateDate(createDate);
        cb.setRate(rate);
        cb.setBody(body);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return cb;
  }

  public List<UserCommentBeans> findCommentByUserId(int id) {
    Connection conn = null;
    List<UserCommentBeans> commentList = new ArrayList<UserCommentBeans>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT sauna.name,comment.rate,comment.body,comment.sauna_id,comment.id from comment INNER join sauna on comment.sauna_id = sauna.id where comment.user_id=?;\n"
              + "";

      // SELECTを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();


      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        String name = rs.getString("name");
        int rate = rs.getInt("rate");
        String body = rs.getString("body");
        int saunaId = rs.getInt("sauna_id");
        int commentId = rs.getInt("id");


        UserCommentBeans userComment = new UserCommentBeans(name, rate, body, saunaId, commentId);
        commentList.add(userComment);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return commentList;
  }

  public List<AvgSaunaRateBeans> selectAvgSaunaRate() {
    Connection conn = null;
    List<AvgSaunaRateBeans> avgRateList = new ArrayList<AvgSaunaRateBeans>();
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT CAST(AVG(CAST(rate as DECIMAL))as DECIMAL(10,1))as rate,sauna_id FROM comment GROUP BY sauna_id;";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);


      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        Float rate = rs.getFloat("rate");
        int saunaId = rs.getInt("sauna_id");
        AvgSaunaRateBeans a = new AvgSaunaRateBeans(rate, saunaId);
        avgRateList.add(a);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return avgRateList;
  }

  public Float findAvgSaunaRateBySaunaId(int id) {
    Connection conn = null;
    Float rate = 0.0f;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT CAST(AVG(CAST(rate as DECIMAL))as DECIMAL(10,1))as rate,sauna_id FROM comment where sauna_id= ?; \n"
              + "";

      // SELECTを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        rate = rs.getFloat("rate");
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return rate;
  }

  public void deleteCommentBySaunaId(int id) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // Delete文を準備
      String sql = "DELETE FROM comment WHERE sauna_id = ?;";

      // Deleteを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      int rs = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}


