package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.SaunaBeans;

public class SaunaDao {
  public List<SaunaBeans> findAllSauna() {
    Connection conn = null;
    List<SaunaBeans> saunaList = new ArrayList<SaunaBeans>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql1 = "SELECT * FROM sauna";


      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql1);


      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String _name = rs.getString("name");
        String _location = rs.getString("location");
        int _price = rs.getInt("price");
        int _sauna_temp = rs.getInt("sauna_temp");
        int _water_temp = rs.getInt("water_temp");

        SaunaBeans sauna = new SaunaBeans();

        sauna.setId(id);
        sauna.setLocation(_location);
        sauna.setName(_name);
        sauna.setPrice(_price);
        sauna.setSauna_temp(_sauna_temp);
        sauna.setWater_temp(_water_temp);

        saunaList.add(sauna);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return saunaList;
  }

  public void insertSauna(String name, String location, int price, int sauna_temp, int water_temp) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // insert文を準備
      String sql =
          "insert into sauna(name,location,price,sauna_temp,water_temp) values(?,?,?,?,?);";

      // insertを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, location);
      pStmt.setInt(3, price);
      pStmt.setInt(4, sauna_temp);
      pStmt.setInt(5, water_temp);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public List<SaunaBeans> search(String name, String location, String priceStart, String priceEnd) {
    Connection conn = null;
    List<SaunaBeans> saunaList = new ArrayList<SaunaBeans>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "select * from sauna where id >= 1";

      StringBuilder sb = new StringBuilder(sql);

      List<String> list = new ArrayList<String>();


      if (!name.equals("")) {
        name = "%" + name + "%";
        sb.append(" and name like ?");
        list.add(name);
      }

      if (!location.equals("")) {
        location = "%" + location + "%";
        sb.append(" and location like ?");
        list.add(location);
      }
      if (!priceStart.equals("")) {
        sb.append(" and price >= ?");
        list.add(priceStart);
      }
      if (!priceEnd.equals("")) {
        sb.append(" and price <= ?");
        list.add(priceEnd);
      }

      System.out.println(sb.toString());
      PreparedStatement pStmt = conn.prepareStatement(sb.toString());
      for (int i = 0; i < list.size(); i++) {
        pStmt.setString(i + 1, list.get(i));
      }
      System.out.println(pStmt.toString());
      ResultSet rs = pStmt.executeQuery();

      System.out.println(rs.toString());

      // if (!rs.next()) {
      // return null;
      // }

      // 結果表に格納されたレコードの内容をUserインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String _name = rs.getString("name");
        String _location = rs.getString("location");
        int _price = rs.getInt("price");
        int _sauna_temp = rs.getInt("sauna_temp");
        int _water_temp = rs.getInt("water_temp");

        System.out.println(_name);
        SaunaBeans sauna = new SaunaBeans();
        sauna.setId(id);
        sauna.setLocation(_location);
        sauna.setName(_name);
        sauna.setPrice(_price);
        sauna.setSauna_temp(_sauna_temp);
        sauna.setWater_temp(_water_temp);
        saunaList.add(sauna);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return saunaList;
  }

  public SaunaBeans findById(int id) {
    Connection conn = null;
    SaunaBeans sauna = new SaunaBeans();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // SELECT文を準備
      String sql = "select * from sauna where id = ?;";

      // SELECTを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を Userインスタンスに追加
      if (!rs.next()) {
        return null;
      }
      String name = rs.getString("name");
      String location = rs.getString("location");
      String _price = rs.getString("price");
      String _sauna_temp = rs.getString("sauna_temp");
      String _water_temp = rs.getString("water_temp");

      int price = 0;
      try {
        price = Integer.valueOf(_price);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }

      int sauna_temp = 0;
      try {
        sauna_temp = Integer.valueOf(_sauna_temp);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }
      int water_temp = 0;
      try {
        water_temp = Integer.valueOf(_water_temp);
      } catch (NumberFormatException ex) {
        ex.printStackTrace();
      }


      sauna.setId(id);
      sauna.setName(name);
      sauna.setLocation(location);
      sauna.setPrice(price);
      sauna.setSauna_temp(sauna_temp);
      sauna.setWater_temp(water_temp);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return sauna;
  }

  public void updateAll(int id, String name, String location, String price, String sauna_temp,
      String water_temp) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // update文を準備
      String sql =
          "update sauna set name =?,location =?,price = ?,sauna_temp = ?,water_temp = ?  where id = ?;";

      // updateを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, location);
      pStmt.setString(3, price);
      pStmt.setString(4, sauna_temp);
      pStmt.setString(5, water_temp);
      pStmt.setInt(6, id);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void deleteSauna(int id) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // Delete文を準備
      String sql = "DELETE FROM sauna WHERE id = ?;";

      // Deleteを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      int rs = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
