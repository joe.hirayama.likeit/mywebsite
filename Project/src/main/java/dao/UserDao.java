package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;
import beans.UserBeans;

public class UserDao {
  public UserBeans findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");

      return new UserBeans(id, _loginId, name, birthDate, _password, isAdmin);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void insert(String loginId, String password, String userName, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // insert文を準備
      String sql = "insert into user(login_id,name,birth_date,password) values(?,?,?,?);";

      // insertを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, userName);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, password);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void updateAll(int id, String password, String userName, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // update文を準備
      String sql =
          "update user set name =?,birth_date =?,password = ? where id = ?;";

      // updateを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      pStmt.setString(2, birthDate);
      pStmt.setString(3, password);
      pStmt.setInt(4, id);
      int result = pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void updateExceptPassword(int id, String userName, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // passwordを除いた、更新をする際のupdate文
      String sql = "update user set name = ?,birth_date = ? where id = ?;";


      // updateを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      pStmt.setString(2, birthDate);
      pStmt.setInt(3, id);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public UserBeans findById(int id) {
    Connection conn = null;
    UserBeans user = new UserBeans();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // SELECT文を準備
      String sql = "select login_id,name,birth_date from user where id = ?;";

      // SELECTを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を Userインスタンスに追加
      if (!rs.next()) {
        return null;
      }
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");


      user.setId(id);
      user.setLoginId(loginId);
      user.setName(name);
      user.setBirthDate(birthDate);



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return user;
  }
}
