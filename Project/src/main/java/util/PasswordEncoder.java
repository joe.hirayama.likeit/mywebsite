package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class PasswordEncoder {
  public String encodePassword(String password) {

    // 変換したい文字列を入力
    String str = password;

    // ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;

    // ハッシュ関数の種類(今回はMD5)
    String algorithm = "MD5";
    String encodeStr = "";

    // ハッシュ生成処理
    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(str.getBytes(charset));

      encodeStr = DatatypeConverter.printHexBinary(bytes);

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    // 暗号化結果をリターンする
    return encodeStr;
  }

}
