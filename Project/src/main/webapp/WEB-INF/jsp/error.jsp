<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

</head>

<body>
     <!-- ヘッダー -->
     <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
            
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="login.html"></a>
                </li>
            </ul>

            <ul class="navbar-nav flex-row">

                <li class="nav-item">
                    <a class="nav-link text-danger" href="Logout">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    
    <h2  class="nav-link text-danger">エラーが発生しました</h2>
    <div class="row">
        <div class="col s12">
            <p class="center-align">
                <a href="SaunaList" class="btn btn-large waves-effect waves-light  col s8 offset-s2">サウナ一覧ページへ</a>
            </p>
        </div>
    </div>
    

   
</body>

</html>