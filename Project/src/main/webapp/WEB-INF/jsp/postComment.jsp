<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<span class="navbar-text">${userInfo.name}さん <a
				href="UserDetail?id=${userInfo.id}"><button type="button"
						class="btn btn-secondary btn-sm">詳細</button></a>
			</span>

			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link"
					href="login.html"></a></li>
			</ul>

			<ul class="navbar-nav flex-row">

				<li class="nav-item"><a class="nav-link text-danger"
					href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">クチコミを投稿</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">

				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>

				<form action="PostComment" method="post">
					<div class="form-group">
						<label for="exampleFormControlTextarea1">投稿者名</label> <input
							type="hidden" name="userName" value="${userInfo.name}"><input
							type="hidden" name="userId" value="${userInfo.id}"><input
							type="hidden" name="saunaId" value="${saunaId}">
						<h3>${userInfo.name}さん</h3>
					</div>
					<div class="form-group">
						<label for="exampleFormControlSelect1">評価(5段階)</label> <select
							class="form-control" id="exampleFormControlSelect1" name="rate">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>

					<div class="form-group">
						<label for="exampleFormControlTextarea1">内容</label>
						<textarea class="form-control" id="exampleFormControlTextarea1"
							rows="3" name="body"></textarea>
					</div>
					<div class="row">
						<div class="col">
							<button type="submit" class="btn btn-primary btn-block">投稿</button>
						</div>
					</div>
				</form>

				<div class="row mt-3">
					<div class="col">
						<a href="SaunaDetail?id=${saunaId}">戻る</a>
					</div>
				</div>
			</div>
		</div>
	</div>



</body>

</html>