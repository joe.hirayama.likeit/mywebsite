<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<span class="navbar-text">${userInfo.name}さん <a
				href="UserDetail?id=${userInfo.id}"><button type="button"
						class="btn btn-secondary btn-sm">詳細</button></a></span>

			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link"
					href="login.html"></a></li>
			</ul>

			<ul class="navbar-nav flex-row">

				<li class="nav-item"><a class="nav-link text-danger"
					href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>


	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<div class="row mb-5 mt-3">
					<div class="col">
						<h1 class="text-center">サウナ削除確認</h1>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<p>サウナ名： ${sauna.name}</p>
						<p>を本当に削除してよろしいでしょうか？</p>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<a href="SaunaList" class="btn btn-light btn-block">いいえ</a>
					</div>
					<div class="col">
						<form method="post" name="form1" action="SaunaDelete">
							<input type="hidden" name="saunaId" value="${sauna.id}">
							<a href="javascript:form1.submit()"
								class="btn btn-primary btn-block">はい</a>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>




</body>

</html>