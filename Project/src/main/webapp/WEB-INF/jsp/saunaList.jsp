<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<span class="navbar-text">${userInfo.name}さん <a
				href="UserDetail?id=${userInfo.id}"><button type="button"
						class="btn btn-secondary btn-sm">詳細</button></a>
			</span>

			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link"
					href="login.html"></a></li>
			</ul>

			<ul class="navbar-nav flex-row">

				<li class="nav-item"><a class="nav-link text-danger"
					href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>


	<p></p>
	<h1 style="text-align: center;">サウナ一覧</h1>
	<c:if test="${userInfo.admin == true}">
		<a href="SaunaAdd">
			<button type="button" class="btn btn-info" style="text-align: right;">サウナ追加</button>
		</a>
	</c:if>
	<div class="card">
		<div class="card-body">
			<form action="SaunaList" method="post">
				<div class="form-group row">
					<label for="userName" class="col-2 col-form-label">サウナ名</label>
					<div class="col-10">
						<input type="text" name="name" class="form-control" id=""
							value="${_name}">
					</div>
				</div>
				<div class="form-group row">
					<label for="userName" class="col-2 col-form-label">所在地</label>
					<div class="col-10">
						<input type="text" name="location" class="form-control" id=""
							value="${_location}">
					</div>
				</div>

				<div class="form-group row">
					<label for="birthDate" class="col-2 col-form-label">料金</label>
					<div class="col-10">
						<div class="row">
							<div class="col-3">
								<input type="text" name="price-start" id="" class="form-control"
									value="${_priceStart}">
							</div>
							<div>円</div>
							<div class="col-2 text-center">
								<font size="4">~</font>
							</div>
							<div class="col-3">
								<input type="text" name="price-end" id="" class="form-control"
									value="${_priceEnd}">
							</div>
							<div>円</div>

						</div>
					</div>
				</div>

				<div class="text-right">
					<button type="submit" class="btn btn-primary form-submit">検索</button>
				</div>
			</form>
		</div>
	</div>

	<table class="table table-striped">
		<thead>
			<tr>
				<th scope="col">サウナ名</th>
				<th scope="col">所在地</th>
				<th scope="col">料金</th>
				<th scope="col">平均評価</th>

			</tr>
		</thead>
		<tbody>
			<c:forEach var="sauna" items="${saunaList}">

					<tr>
						<td scope="row">${sauna.name}</td>
						<td>${sauna.location}</td>
						<td>${sauna.price}円</td>
						<td>${sauna.avgRate}</td>

						<td style="text-align: right;"><a
							href="SaunaDetail?id=${sauna.id}"><button type="button"
									class="btn btn-outline-primary">詳細</button></a> <c:if
								test="${userInfo.admin == true}">
								<a href="SaunaUpdate?id=${sauna.id}"><button type="button"
										class="btn btn-outline-success">編集</button></a>
								<a href="SaunaDelete?id=${sauna.id}"><button type="button"
										class="btn btn-outline-danger">削除</button></a>
							</c:if></td>
					</tr>
				</c:forEach>
		</tbody>
	</table>
</body>

</html>