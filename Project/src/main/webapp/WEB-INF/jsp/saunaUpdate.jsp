<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<link href="css/saunaAddForm.css" rel="stylesheet" type="text/css" />

</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<span class="navbar-text">${userInfo.name}さん <a
				href="UserDetail?id=${userInfo.id}"><button type="button"
						class="btn btn-secondary btn-sm">詳細</button></a>
			</span>

			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link"
					href="login.html"></a></li>
			</ul>

			<ul class="navbar-nav flex-row">

				<li class="nav-item"><a class="nav-link text-danger"
					href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">サウナ情報更新</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3 mb-5">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
			</div>
		</div>

		<form action="SaunaUpdate" method="post">
			<div class="buttonwrap">
				<input type="hidden" name="id" value="${sauna.id}">
				<table align="center">

					<tr>
						<th><label>サウナ名</label></th>
						<td><input type="text" name="name" value="${sauna.name}"></td>
					</tr>
					<tr>
						<th><label>所在地</label></th>
						<td><input type="text" name="location"
							value="${sauna.location}"><span class="attention"></span></td>
					</tr>
					<tr>
						<th><label>料金</label></th>
						<td><input type="text" name="price" class="smallinput"
							value="${sauna.price}">円</td>
					</tr>
					<tr>
						<th><label>サウナ温度</label></th>
						<td><input type="text" name="sauna_temp" class="smallinput"
							value="${sauna.sauna_temp}">度</td>
					</tr>
					<tr>
						<th><label>水風呂温度</label></th>
						<td><input type="text" name="water_temp" class="smallinput"
							value="${sauna.water_temp}">度</td>
					</tr>
				</table>
				<a href="SaunaList"><input type="button" value="戻る"></a> <input
					type="submit" value="追加">
			</div>
		</form>
	</div>
</body>

</html>