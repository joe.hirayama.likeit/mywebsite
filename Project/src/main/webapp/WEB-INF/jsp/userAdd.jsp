<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
            <span class="navbar-text" style="display: contents;" >

            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
               
            </ul>

            <ul class="navbar-nav flex-row">

                    <p style="text-align:center"><a class="nav-link text-danger"  href="Logout">ログアウト</a></p>
            </ul>
        </nav>
    </header>
    <div class="row">
        <div class="col-6 offset-3 mb-5">
            <c:if test="${errMsg != null}">
                <div class="alert alert-danger" role="alert">入力された内容は正しくありません</div>
            </c:if>
        </div>
    </div>

    <div class="row">
        <div class="col-6 offset-3">
            <form action="UserAdd" method="post">
                <div class="form-group row">


                    <label for="user-id" class="control-label col-3">ログインID</label>
                    <div class="col-9">
                        <input type="text" name="loginId" id="user-loginid"
                            class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="control-label col-3">パスワード</label>
                    <div class="col-9">
                        <input type="password" name="password" id="password"
                            class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password-confirm" class="control-label col-3">パスワード(確認)</label>
                    <div class="col-9">
                        <input type="password" name="password-confirm"
                            id="password-confirm" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user-name" class="control-label col-3">ユーザ名</label>
                    <div class="col-9">
                        <input type="text" name="name" id="user-name"
                            class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="continent" class="control-label col-3">生年月日</label>

                    <div class="col-9">
                        <input type="date" name="birth-date" id="birth-day"
                            class="form-control" size="30" value="${birthDate}">
                    </div>
                </div>

                <div>
                    <button type="submit" value="登録"
                        class="btn btn-primary btn-block form-submit">登録</button>
                </div>
            </form>

            <div class="row mt-3">
                <div class="col">
                    <a href="Login">戻る</a>
                </div>
            </div>
        </div>
    </div>


    
</body>

</html>