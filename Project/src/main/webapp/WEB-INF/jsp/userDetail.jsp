<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<span class="navbar-text">${userInfo.name}さん <a
				href="UserDetail?id=${userInfo.id}"><button type="button"
						class="btn btn-secondary btn-sm">詳細</button></a>
			</span>

			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active"><a class="nav-link"
					href="login.html"></a></li>
			</ul>

			<ul class="navbar-nav flex-row">

				<li class="nav-item"><a class="nav-link text-danger"
					href="Logout">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">ユーザ詳細情報</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">

				<div class="row">
					<label for="loginId" class="col-3 font-weight-bold">ログインID</label>
					<div class="col-9">
						<p>${userInfo.loginId}</p>
					</div>
				</div>

				<div class="row">
					<label for="userName" class="col-3 font-weight-bold">ユーザ名</label>
					<div class="col-9">
						<p>${userInfo.name}</p>
					</div>
				</div>

				<div class="row">
					<label for="birthDate" class="col-3 font-weight-bold"
						pattern="yyyy年MM月dd日">生年月日</label>
					<div class="col-9">
						<p>
							<fmt:formatDate value="${userInfo.birthDate}"
								pattern="yyyy年MM月dd日" />
						</p>
					</div>
				</div>



				<div class="row">
					<label for="userName" class="col-3 font-weight-bold">投稿したクチコミ</label>
					<div class="col-9">
						<table class="table table-striped">
							<thead>
								<tr>
									<th scope="col"></th>
									<th scope="col">サウナ名</th>
									<th scope="col">評価</th>
									<th scope="col">内容</th>
								</tr>
							</thead>
							<c:forEach var="comment" items="${commentList}">
								<tbody>
									<tr>
										<th scope="row"></th>
										<td><a href="SaunaDetail?id=${comment.saunaId}">${comment.saunaName}</a></td>

										<td>${comment.rate}</td>
										<td>${comment.body}</td>
										<td><a
											href="CommentDelete?commentId=${comment.commentId}&saunaId=${comment.saunaId}"><button
													type="button" class="btn btn-outline-danger">削除</button></a></td>
									</tr>
								</tbody>
							</c:forEach>
						</table>
					</div>
				</div>

				<div class="col-xs-4">
					<a href="SaunaList">戻る</a>
				</div>

			</div>
			<a href="UserUpdate"><button type="button"
					class="btn btn-outline-info" style="text-align: right">編集する</button></a>

		</div>
	</div>


</body>

</html>